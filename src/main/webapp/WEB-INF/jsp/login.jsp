<%@ page language="java" contentType="text/html; charset=UTF-8" 	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${app} Login</title>
	
	

	<spring:url value="/resources/css/bootstrap-material-design.min.css" var="materialCss" />
	<spring:url value="/resources/css/bootstrap.min.css" var="bootstrapCss" />
	<spring:url value="/resources/css/ripples.css" var="ripplesMinCss" />
	<spring:url value="/resources/css/roboto.css" var="robotoCss" />
	<spring:url value="/resources/css/material_icon.css" var="materialIconCss" />


	<spring:url value="/resources/js/jquery.js" var="jqueryJs"/>
	<spring:url value="/resources/js/material.js" var="materialJs"/>
	<spring:url value="/resources/js/ripples.js" var="ripplesJs"/>
	
		
	<link href="${materialCss}" rel="stylesheet" />
	<link href="${bootstrapCss}" rel="stylesheet" />
	<link href="${ripplesMinCss}" rel="stylesheet" />
	<link href="${robotoCss}" rel="stylesheet" />
  	<link href="${materialIconCss}" rel="stylesheet" />
    
    <script src="${jqueryJs}"></script>
    <script src="${materialJs}"></script>
    <script src="${ripplesJs}"></script>
    <script>
    $(document).ready(function(){
    	$.material.init();
    });
    </script>
    


</head>
<body>
<div class="container-fluid main">
	<div class="row">
	<div class="col-lg-4">
		<div class="well" style="padding:20px;">
			<h5><spring:message code="add.note" /></h5>
		
			<form action="" method="post">
				<input type="text" name="name" class="form-control"  placeHolder="<spring:message code="username" />">
				<input type="password" name="password" class="form-control"  placeHolder="<spring:message code="password" />">
				<br/>
				<span style="color:red">${error}</span>
				<br/>
				<button class="btn btn-raised btn-primary col-lg-12"><spring:message code="login" /></button>
			</form>
		</div>
	</div>
	</div>
	
</div>	
	
</body>
</html>