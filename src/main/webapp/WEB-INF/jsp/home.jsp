<%@ page language="java" contentType="text/html; charset=UTF-8" 	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${app} Home</title>
	
	

	<spring:url value="/resources/css/bootstrap-material-design.min.css" var="materialCss" />
	<spring:url value="/resources/css/bootstrap.min.css" var="bootstrapCss" />
	<spring:url value="/resources/css/ripples.css" var="ripplesMinCss" />
	<spring:url value="/resources/css/roboto.css" var="robotoCss" />
	<spring:url value="/resources/css/material_icon.css" var="materialIconCss" />


	<spring:url value="/resources/js/jquery.js" var="jqueryJs"/>
	<spring:url value="/resources/js/material.js" var="materialJs"/>
	<spring:url value="/resources/js/bootstrap.min.js" var="bootstrapJs" />
	<spring:url value="/resources/js/b.js" var="bJs" />
	<spring:url value="/resources/js/ripples.js" var="ripplesJs"/>
	
		
	<link href="${materialCss}" rel="stylesheet" />
	<link href="${bootstrapCss}" rel="stylesheet" />
	<link href="${ripplesMinCss}" rel="stylesheet" />
	<link href="${robotoCss}" rel="stylesheet" />
  	<link href="${materialIconCss}" rel="stylesheet" />
    
    <script src="${jqueryJs}"></script>
    <script src="${materialJs}"></script>
    <script src="${bootstrapJs}"></script>
    <script src="${bJs}"></script>
    <script src="${ripplesJs}"></script>
    
    <script>
    $(document).ready(function(){
    	$.material.init();
    });
    var notes = 0;
    function addNote(){
    	var noteHtml = '<div class="col-lg-4">' +
							'<textarea rows="10" class="form-control" placeHolder="Note" />' +
    					'</div>';
    	$("#content").append(noteHtml);
    	$.material.init();
    }
    function showAddNote(){
    	$("#myModal").modal();
    }
    
    function submitNote(){
    	$.post("note", $("#addNoteForm").serialize());
    }
    
    $(document).ready(function(){
    	$("#uploadImageButton").click(function(){
    		$("#uploadImage").trigger( "click" );
    	});
    	$("#uploadImage").change(function(){
    		$("form#uploadImageForm").submit();
    	});
    	
    	$("form#uploadImageForm").submit(function(event){
    		$("#idRestoreLoadingIcon").show();
    		event.preventDefault();
    	    var formData = new FormData($(this)[0]);
        
    		console.log("DATA.....");
    		console.log(formData);
    		console.log("EOD.....");
    		$.ajax({
                url: 'upload_image',
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                	json = JSON.parse(result);
                    alert("result : " + json["path"]);
                    
                },
                error: function(){
                    alert("error in ajax form submission");
                }
            });
    		return false;
        });
    });
    
    
    
    </script>
    


</head>
<body>
<div class="container-fluid main">
	<div class="row">
		<div class="well" style="background-color:#007FFF">
			<span style='float:left;'><h4><a href='home' style='color:#FFFFFF'><spring:message code="app.name" /> </a></h4></span>
			<span style='text-align:right;color:#FFFFFF'><h5><spring:message code="hello" /> ${ name } <a style='color:#FFFFFF' href='logout.html'><spring:message code="logout" /></a></h5></span>
		</div>
	</div>
	<form method="post" id="uploadImageForm" action="upload_image" enctype="multipart/form-data" style="display:none" >
		<input type="file" name="file" id="uploadImage" >
		<button id="xyz">Submit</button>
	</form>
	<h4><spring:message code="add.note" /></h4>
	<a href="?language=en">English</a> | <a href="?language=es">Spanish</a>
	 <div class="modal" id='myModal'>
	 	<div class="modal-dialog">
	 		<form:form commandName="note" id="addNoteForm">
	    		<div class="modal-content">
	        		<div class="modal-header">
	        			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        			<h4 class="modal-title"><spring:message code="add.note" /></h4>
	        			<form:input path="title" class="form-control"/> <br/>
	        			<form:textarea path="note" rows="15" class="form-control"/>
	        			<button type="button" id="uploadImageButton">+</button>
	      			</div>
	      			<div class="modal-body">
	        		
	        			<button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Cancel</button>
	        			<button type="button" class="btn btn-raised btn-success" onclick="submitNote()">Save changes</button>
	      			</div>
	    		</div>
	    	</form:form>
	    	
	  	</div>
	</div>
    <div class="row" id="content">
    	<div class="row" style="padding:10px;margin:10px;">
	      	<c:forEach var="note" items="${notes}">
	      		<div class="col-lg-4">
	      			<div class="well">
						<h4><b>${note.title}</b></h4>
						<p>
							${fn:replace(note.note, newLineChar,'<br />')}
						</p>
					</div>
				</div>
			</c:forEach>
		</div>
		
	</div>
	<div style="position:fixed;left:90%;top:90%;">
		<a href="javascript:showAddNote()" class="btn btn-info btn-fab"><i class="material-icons">add</i></a>
	</div>
</div>	
	
	
	
</body>
</html>