package com.easwareapps.notes.service;

import com.easwareapps.notes.entity.Users;

public interface LoginService {
	
	Users validateUser(Users user);
	public Users findByUserCode(long userCode);

}
