package com.easwareapps.notes.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easwareapps.notes.entity.Users;
import com.easwareapps.notes.repository.UserRepository;



@Service
public class LoginServiceImpl implements LoginService{

	@PersistenceContext
	private EntityManager entityManagerFactory;
	
	@Autowired
	UserRepository userRepository;
	
	public Users findByUserCode(long userCode) {
		return userRepository.findOne(userCode);
		
	}
	
	@Override
	public Users validateUser(Users user) {

		List<Users> users = userRepository.findUserByUnameAndPasswordAndStatus(user.getUname(), user.getPassword(), 1);

	    if(users.size() == 1) {
	    	return users.get(0);
	    }
	    return null;

	    
	}
	
	
}


