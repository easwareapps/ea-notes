package com.easwareapps.notes.service;

import java.util.List;

import com.easwareapps.notes.entity.Note;
import com.easwareapps.notes.entity.NoteContent;

public interface NoteService {

	Long saveNote(Note note);
	void deleteNote(Long id);
	//boolean addNoteEntry(Long id, )
	Long saveNoteContent(NoteContent details);
	List<Note> getAllNotes();
	List<NoteContent> getNoteContents(Long id);
}
