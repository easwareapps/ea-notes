package com.easwareapps.notes.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easwareapps.notes.entity.FullNote;
import com.easwareapps.notes.entity.Note;
import com.easwareapps.notes.entity.NoteContent;
import com.easwareapps.notes.repository.NoteContentRepository;
import com.easwareapps.notes.repository.NoteRepository;

@Service
public class NoteServiceImpl implements NoteService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	NoteRepository noteRepo;
	
	@Autowired
	NoteContentRepository noteContentRepo;
	
	public Long saveNote(Note note) {
		noteRepo.save(note);
		noteRepo.flush();
		return note.getId();
	}
	
	public void deleteNote(Long id) {
		noteRepo.delete(id);
		noteContentRepo.deleteNoteContentByNoteId(id);
	}
	
	
	public Long saveNoteContent(NoteContent details) {
		noteContentRepo.save(details);
		noteContentRepo.flush();
		return details.getId();
	}
	
	public List<Note> getAllNotes() {
		Query fullNotesQuery = entityManager.createQuery("SELECT note from Note note");
		
		return fullNotesQuery.getResultList();
	}
	
	public List<NoteContent> getNoteContents(Long id) {
		Query noteContentQuery = entityManager.createQuery("SELECT content from NoteContent content where content.noteId=?1", NoteContent.class);
		noteContentQuery.setParameter("1", id);
		
		return noteContentQuery.getResultList();
		
	}
	
	
	

}
