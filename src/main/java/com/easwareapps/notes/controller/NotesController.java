package com.easwareapps.notes.controller;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;

import com.easwareapps.notes.entity.FullNote;
import com.easwareapps.notes.entity.Note;
import com.easwareapps.notes.entity.NoteContent;
import com.easwareapps.notes.service.NoteService;



@RestController
public class NotesController implements ServletContextAware {


	@Autowired
	NoteService  noteService;
	
	
	ServletContext servletContext;

	private @Autowired HttpServletRequest request;
	
	@GetMapping("/note")
	public ResponseEntity<List<FullNote>> getNotes(HttpSession session) {

		List<FullNote> fullNotes = new ArrayList<>();
		List<Note> notes = noteService.getAllNotes();
		for (Note note : notes) {
			FullNote tmpFullNote = new FullNote();
			tmpFullNote.setId(note.getId());
			tmpFullNote.setTitle(note.getTitle());
			List<NoteContent> contents = noteService.getNoteContents(note.getId());
			if(contents != null ){
				for(NoteContent content : contents) {
					if(content.getType() == NoteContent.TEXT) {
						tmpFullNote.setNote(content.getContent());
					}
				}
			}


			fullNotes.add(tmpFullNote);

		}

		return new ResponseEntity<List<FullNote>>(fullNotes, HttpStatus.OK);

	}

	@GetMapping("/note/{id}")
	public ResponseEntity<List<NoteContent>> getNoteContent(HttpSession session,
			@PathVariable Long id) {

		List<NoteContent> contents = noteService.getNoteContents(id);

		return new ResponseEntity<List<NoteContent>>(contents, HttpStatus.OK);

	}

	@PostMapping(value="/note")
	public String addNote(HttpSession session,
			@ModelAttribute FullNote fullnote){
		System.out.println("Hello Got Note" +  fullnote.getTitle() + "\n" + fullnote.getNote());

		Note note = new Note();
		//note.setId((long)0);
		note.setStatus(1);
		note.setOwner((long)1);
		note.setModificationTime((long)0);
		note.setCreatedTime((long) 0);
		note.setTitle(fullnote.getTitle());
		noteService.saveNote(note);


		NoteContent details = new NoteContent();
		details.setType(NoteContent.TEXT);
		details.setContent(fullnote.getNote());
		details.setNoteId(note.getId());
		details.setStatus(1);
		//details.setValue(" ");
		noteService.saveNoteContent(details);

		return "";
	}

	@DeleteMapping(value="/note/{id}")
	public String deleteNote(@PathVariable Long id) {

		noteService.deleteNote(id);

		return "";
	}

	@PutMapping(value="/note")
	public String updateNote(@ModelAttribute FullNote fullnote){
		Note note = new Note();
		//note.setId((long)0);
		note.setStatus(1);
		note.setOwner((long)1);
		note.setModificationTime((long)0);
		note.setCreatedTime((long) 0);
		note.setTitle(fullnote.getTitle());
		noteService.saveNote(note);
		return "";
	}
	
	
	

	@PostMapping(value="/upload_image")
	@RequestMapping(value="/upload_image")
	public String uploadImage(HttpServletRequest request, 
			@RequestParam(value = "file", required = false) MultipartFile image) {                 

		
		String uploadsPath = request.getSession().getServletContext().getRealPath("/") + "/resources/uploads/";
		File dir = new File(uploadsPath);
		String ext = ".xxx";
		try{
			if( !dir.exists() ) {
				if( !dir.mkdirs() ){
					return "Error can't create directory";
				}
			}
			ext = FilenameUtils.getExtension(image.getOriginalFilename());
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error 0");
			
		}
		String name = "file_" + Math.random()%9999 + "." + ext;
		File file = new File(dir, name);
		if (image != null && !image.isEmpty()) {
			try{
				FileCopyUtils.copy(image.getBytes(), file);
				return "{\"status\":\"ok\", \"path\":\"/resources/uploads/"  + name + "\"}";
			}catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error 1");
			}
		}
		return "{\"sstatus\": \"error\"}";
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		
	}
	
	//Sort s = new Sort(Sort.Direction.DESC, "title");
	//	org.springframework.data.domain.Pageable p = new PageRequest(0, 2, s);
	
	//TypedQuery query = em.createQuery("select a from User a where a.uname = ?1 and a.password= ?2", User.class);
	//    query.setParameter(1, user.getUname());
	//    query.setParameter(2, user.getPassword());
    //
	//    try{
	//    	return (User)query.getResultList().get(0);
	//    }catch (Exception e) {
	//		return null;
	//	}

	

}
