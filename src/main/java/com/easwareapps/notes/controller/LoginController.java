package com.easwareapps.notes.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easwareapps.notes.entity.Users;
import com.easwareapps.notes.service.LoginService;



@Controller
public class LoginController {


	
	@Autowired
	LoginService loginService;
	
	@RequestMapping(value="/login", method = { RequestMethod.GET })
	public String login(Model model) {
		System.out.println("Hello on Login GET");
		return "login";
		
	}
	
	
	
	@RequestMapping(value="/login", method = { RequestMethod.POST })
	public String validateLogin(
				@RequestParam (value="name") String name,
				@RequestParam (value="password") String password, 
				HttpSession session,
				Model model
			) {
		try{
			if(session != null &&  session.getAttribute("user") !=null &&
					! session.getAttribute("user").equals("")) {
				return "redirect:/home";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		password = DigestUtils.md5DigestAsHex(password.getBytes());
		//System.out.println("DATA: " + name + " : " + password );
		Users user = new Users();
		user.setUname(name);
		user.setPassword(password);
		Users result = loginService.validateUser(user);
		if( result != null ){
			session.setAttribute("user", result.getName());
			return "redirect:/home.html";
		}
		model.addAttribute("error", "Invalid Credentials");
		return "login";
		
	}
	
	@RequestMapping(value="/logout")
	public String logout(HttpSession session) {
		try{
			session.invalidate();
		}catch (Exception e) {
			
		}
		
		return "redirect:/login.html";
	}
}
