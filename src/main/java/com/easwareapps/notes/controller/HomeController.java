package com.easwareapps.notes.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.easwareapps.notes.entity.FullNote;
import com.easwareapps.notes.entity.Note;
import com.easwareapps.notes.entity.NoteContent;
import com.easwareapps.notes.service.NoteService;


@Controller
//@SessionAttributes("user")
public class HomeController {

	@Autowired
	NoteService noteService;
	
	@RequestMapping(value="/home")
	public String home(HttpSession session, Model view) {
		
		String name= "";
		try{
			name = session.getAttribute("user").toString();
			//System.out.println(note.getTitle());
		}catch (Exception e) {
			return "redirect:/login.html";
		}
		if (name.equals("")){
			return "redirect:/login.html";
		}
		
		
		view.addAttribute("name", name);
		view.addAttribute("note", new FullNote());
		
		return "home";
		
	}
	
	
}