package com.easwareapps.notes;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class EaNoteInitializer implements WebApplicationInitializer {


	@Override
	public void onStartup(ServletContext servletContext) 
			throws ServletException {
		
		WebApplicationContext context = getContext();
		servletContext.addListener(new ContextLoaderListener(context));
		ServletRegistration.Dynamic dispatcher = 
				servletContext.addServlet("EANoteServlet", 
						new DispatcherServlet(context));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("*.html");
//		dispatcher.addMapping("*.css");
//		dispatcher.addMapping("*.js");
		dispatcher.addMapping("/");
		dispatcher.addMapping("/resources/");

	}

	private AnnotationConfigWebApplicationContext getContext() {
		AnnotationConfigWebApplicationContext context = 
				new AnnotationConfigWebApplicationContext();
		context.setConfigLocation("com.easwareapps.WebConfig");
		context.register(WebConfig.class);
		return context;
	}
	
	

}
