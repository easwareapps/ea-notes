package com.easwareapps.notes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easwareapps.notes.entity.Users;


public interface UserRepository extends JpaRepository<Users, Long> {

	List<Users> findUserByUnameAndPasswordAndStatus(String uname, String password, int status);
}
