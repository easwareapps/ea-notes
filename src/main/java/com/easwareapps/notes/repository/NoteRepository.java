package com.easwareapps.notes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easwareapps.notes.entity.Note;

public interface NoteRepository extends JpaRepository<Note, Long> {

    @Query("select n from Note n where n.owner=:owner")
	Page<Note> queryByOwner(@Param("owner") long owner, Pageable p);
	
}
