package com.easwareapps.notes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.easwareapps.notes.entity.NoteContent;

public interface NoteContentRepository extends JpaRepository<NoteContent, Long>{

	void deleteNoteContentByNoteId(Long id);
}
