package com.easwareapps.notes.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Note")
public class Note {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String title;
	private Long owner;
	
	@Column(name="creation_time")
	private Long creationTime;
	
	@Column(name="modification_time")
	private Long modificationTime;
	
	private int status;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getCreatedTime() {
		return creationTime;
	}
	public void setCreatedTime(Long createdTime) {
		this.creationTime = createdTime;
	}
	public Long getOwner() {
		return owner;
	}
	public void setOwner(Long owner) {
		this.owner = owner;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Long getModificationTime() {
		return modificationTime;
	}
	public void setModificationTime(Long modificationTime) {
		this.modificationTime = modificationTime;
	}
	
	
	
}